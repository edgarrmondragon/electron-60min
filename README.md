# Build an Electron App in Under 60 Minutes

In this video we will build a desktop application using Electron.js which allows us to build desktop apps using only JavaScript. We will build a minimal shopping list app with a custom menu and multiple windows and implement Materialize CSS for styling. We will also publish the app to a Windows .exe file using the Electron Packager.

https://www.youtube.com/watch?v=kN1Czs0m1SU
